package seleniumapi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class DropDwonTest {
    WebDriver driver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/Users/typer/Desktop/PragmaticQA/Selenium/webDrivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://pragmatic.bg/automation/lecture13/DragDropDemo.html");
    }

    @Test
    public void testDopDownMenu() throws InterruptedException {
        WebElement source = driver.findElement(By.id("draggable"));
        WebElement target = driver.findElement(By.id("droppable"));

        Actions builder = new Actions(driver);

        //Never forget .parform() -> this method run the builder
        builder.dragAndDrop(source, target).perform();

        assertEquals(target
        .getText(), "Dropped!");
    }

    @AfterMethod
    public void turnDown(){
        driver.close();
    }
}
