package seleniumapi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ElementTest {

    WebDriver driver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/Users/typer/Desktop/PragmaticQA/Selenium/webDrivers/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://zamunda.net");
    }

    @Test
    public void testElement() throws InterruptedException {
        //Get the message Element
        WebElement message = driver.findElement(By.id("message"));
        //Get the message elements text
        String messageText = message.getText();
        Assert.assertEquals(messageText, "Click on me and my color will change",
                "this will appear if expected and actual are not the same");

        Assert.assertTrue(messageText.contains("color"), "nqma q be, bug");
        Thread.sleep(1000);


        //Verify message element's text displays "Click on me and my color will change"
        //Assert.assertEquals("Click on me and my color will change", messageText);

        //Get the area Element
        WebElement area = driver.findElement(By.id("area"));

        //Verify area element's text displays "Div's Text\nSpan's Text"
        Assert.assertEquals(area.getText(), "Div's Text\nSpan's Text",  "bug beee");
    }

    @AfterMethod
    public void turnDown(){
        driver.close();
    }
}
