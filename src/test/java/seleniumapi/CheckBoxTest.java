package seleniumapi;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class CheckBoxTest {

    private static WebDriver driver;

    @BeforeMethod
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/Users/typer/Desktop/PragmaticQA/Selenium/webDrivers/chromedriver");
        this.driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        this.driver.get("http://pragmatic.bg/automation/lecture13/Config.html");
    }

    @Test
    public void testCheckBox() throws InterruptedException {
        //Get the Checkbox as WebElement using it's value attribute
        WebElement airbags = driver.findElement(By.xpath("//input[@value='Airbags']"));

        //Check if its already selected? otherwise select the Checkbox
        //by calling click() method
        if (!airbags.isSelected())
            airbags.click();

        //Verify Checkbox is Selected
        assertTrue(airbags.isSelected());

        Thread.sleep(1000);

        //Check Checkbox if selected? If yes, deselect it
        //by calling click() method
        if(airbags.isSelected()){
            airbags.click();
        }

        //Verify Checkbox is Deselected
        assertFalse(airbags.isSelected());
        Thread.sleep(1000);

    }

    @AfterMethod
    public void turnDown(){
        driver.quit();
        driver.close();
    }
}
