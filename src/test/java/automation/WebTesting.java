package automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class WebTesting {

    WebDriver driver;

    @BeforeMethod
    public void setUpPage()throws InterruptedException{

        System.setProperty("webdriver.chrome.driver", "/Users/typer/Desktop/PragmaticQA/Selenium/webDrivers/chromedriver");
        this.driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        this.driver.get("http://google.com/");
    }

    @Test
    public void testName() {
        WebElement element = driver.findElement(By.cssSelector(".gLFyf"));
//        element.click();
        element.sendKeys("Zaharina Ivanov Angelov");
        element.submit();
    }


    @AfterMethod
    public void afterTest(){
        driver.quit();
    }

}